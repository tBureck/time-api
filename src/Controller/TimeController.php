<?php

namespace TBureck\TimeApi\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class TimeController
 * @package TBureck\TimeApi\Controller
 *
 * @author Tim Bureck
 * @since 2018-01-08
 */
class TimeController extends Controller
{

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->json([
            'time' => (new \DateTime('now', new \DateTimeZone('UTC')))->format('c')
        ]);
    }
}